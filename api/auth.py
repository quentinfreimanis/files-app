import hashlib
import logging
import os
import time

import jwt

jwt_key = os.urandom(32)
TOKEN_VALID_SECS = 86400

def hash(password, salt=None):
    if salt == None:
        salt = os.urandom(32)
    key = hashlib.pbkdf2_hmac(
        'sha256', # The hash digest algorithm for HMAC
        password.encode('utf-8'), # Convert the password to bytes
        salt, # Provide the salt
        100000 # It is recommended to use at least 100,000 iterations of SHA-256 
        )
    return key, salt

def encode_jwt(user_id, username, is_admin):
    token = jwt.encode({
                "sub": user_id, 
                "name": username,
                "iat": int(time.time()), 
                "exp": int(time.time()) + TOKEN_VALID_SECS, 
                "iss": "q-lab:files",
                "admin": is_admin
                },
            jwt_key, algorithm="HS256")
    return token

def decode_jwt(token):

    data = jwt.decode(token, jwt_key, issuer="q-lab:files", algorithms=["HS256"])

    # check if token is still valid
    if data["exp"] <= int(time.time()):
        raise jwt.ExpiredSignature
    else:
        return data