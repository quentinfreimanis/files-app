from flask import jsonify

def userpassincorrect():
    response = jsonify({"status": "error", "response": "userpassincorrect", "description": "Username or password incorrect"})
    response.status_code = 401
    return response

def logintoken(token):
    response = jsonify({"status": "ok", "response": "login", "description": "Successfully logged in"})
    response.set_cookie("files-jwt", token, httponly=True, samesite="Strict")
    response.status_code = 200
    return response

def logout():
    response = jsonify({"status": "ok", "response": "logout", "description": "Successfully logged out"})
    response.set_cookie("files-jwt", "null", httponly=True, samesite="Strict", expires=0)
    response.status_code = 200
    return response

def insfperms():
    response = jsonify({"status": "error", "response": "insufficientperms", "description": "Insufficient Permissions"})
    response.status_code = 403
    return response

def usertaken():
    response = jsonify({"status": "error", "response": "usernametaken", "description": "Username already in use"})
    response.status_code = 409
    return response

def newuser():
    response = jsonify({"status": "ok", "response": "usercreated", "description": "Successfully created a new user"})
    response.status_code = 201
    return response

def deleteuser():
    response = jsonify({"status": "ok", "response": "userremoved", "description": "Successfully removed user(s)"})
    response.status_code = 200
    return response

def invalidtoken():
    response = jsonify({"status": "error", "response": "invalidtoken", "description": "Login token is invalid or expired"})
    response.status_code = 403
    return response

def userlist(users):
    ser_users = [i.serialize() for i in users]
    response = jsonify({"status": "ok", "response": ser_users, "description": "List of users"})
    response.status_code = 200
    return response

def quotalimit():
    response = jsonify({"status": "error", "response": "quotalimit", "description": "User exceeds quota"})
    response.status_code = 413
    return response

def filelimit():
    response = jsonify({"status": "error", "response": "filelimit", "description": "File too large"})
    response.status_code = 413
    return response

def uploaded():
    response = jsonify({"status": "ok", "response": "uploaded", "description": "Successfully Uploaded new file"})
    response.status_code = 201
    return response

def filelist(items):
    ser_items = [i.serialize() for i in items]
    response = jsonify({"status": "ok", "response": ser_items, "description": "List of Files"})
    response.status_code = 200
    return response

def deletefile():
    response = jsonify({"status": "ok", "response": "fileremoved", "description": "Successfully removed File(s)"})
    response.status_code = 200
    return response

def login():
    response = jsonify({"status": "ok", "response": "logintrue", "description": "User is logged in"})
    response.status_code = 200
    return response

def unprocessableentity():
    response = jsonify({"status": "error", "response": "unprocessableentity", "description": "The request was well-formed but was unable to be followed due to semantic errors"})
    response.status_code = 422
    return response