import time
import os
import logging
import uuid
import shutil

import jwt

from apscheduler.schedulers.background import BackgroundScheduler

from flask import Flask, jsonify, send_file
from flask_restful import Resource, Api, request
from flask_sqlalchemy import SQLAlchemy

from webargs import fields
from webargs.flaskparser import use_args

from werkzeug.utils import secure_filename

import auth
import responses
import wargs

app = Flask(__name__)
api = Api(app)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("POSTGRES_URL")
app.config['UPLOAD_FOLDER'] = os.environ.get("UPLOAD_FOLDER")
app.config['MAX_CONTENT_LENGTH'] = int(os.environ.get("MAX_UPLOAD")) * 1024 * 1024

MAX_USER_FILES = int(os.environ.get("MAX_USER_FILES"))
USER_QUOTA = int(os.environ.get("USER_QUOTA")) / 1024 / 1024
DEFAULT_TTL = lambda: int(os.environ.get("DEFAULT_TTL")) + int(time.time())

db = SQLAlchemy(app)

class QuotaLimit(Exception):
    pass

class FileLimit(Exception):
    pass

class User(db.Model):
    """User Table"""

    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), unique=True)
    password = db.Column(db.LargeBinary(32))
    salt = db.Column(db.LargeBinary(32))
    admin = db.Column(db.Boolean, default=False)
    file_id = db.relationship("File", backref="user", lazy=True)

    def __repr__(self):
        return f"<User: {self.id} {self.username}>"

    def serialize(self):
        return {
            "id": self.id,
            "username": self.username,
            "admin": self.admin,
        }

class File(db.Model):
    """File Metadata"""

    __tablename__ = "file"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    path = db.Column(db.String(128))
    mimetype = db.Column(db.String(128))
    size = db.Column(db.Integer)
    expire_at = db.Column(db.BigInteger)
    upload_time = db.Column(db.BigInteger)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self):
        return f"<user_id: {self.user_id} filename: {self.name[:32]} path: {self.path[:32]} upload_time: {self.upload_time} expire_at: {self.expire_at}>"

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "path": self.path,
            "mimetype": self.mimetype,
            "size": self.size,
            "ttl": self.expire_at,
            "upload_time": self.upload_time,
            "user_id": self.user_id
        }

#db.create_all()
#db.session.commit()

#key, salt = auth.hash("password@123")
#new_user = User(username="admin", password=key, salt=salt, admin=True)
#db.session.add(new_user)
#db.session.commit()

def ttl_cleanup():
    app.logger.info("Removing expired user files")

    files = File.query.filter((File.expire_at < time.time()) & (File.expire_at != 0))
    allfiles = files.all()

    files.delete()
    db.session.commit()

    app.logger.debug(allfiles)

    for userfile in allfiles:
        os.remove(os.path.join(app.config["UPLOAD_FOLDER"], userfile.path))


@app.errorhandler(422)
def handle_422(err):
    return responses.unprocessableentity()

class Login(Resource):

    @use_args(wargs.logintoken, location="cookies")
    def get(self, cookies):
        """Test if user is logged in"""

        # valadate jwt
        try:
            data = auth.decode_jwt(cookies["files-jwt"])
        except:
            return responses.invalidtoken()

        return responses.login()


    @use_args(wargs.userpass, location="json")
    def post(self, args):

        app.logger.info("logged user !!!!!!!!")

        db_user = User.query.filter_by(username=args["username"]).first()

        # chek if user exits in db
        if db_user == None:
            return responses.userpassincorrect()

        if db_user.password == auth.hash(args["password"], db_user.salt)[0]:
            # username and password are correct
            token = auth.encode_jwt(db_user.id, db_user.username, db_user.admin)
            return responses.logintoken(token)
        else:
            return responses.userpassincorrect()

class Logout(Resource):

    def get(self):
        return responses.logout()

class Users(Resource):

    @use_args(wargs.logintoken, location="cookies")
    def get(self, cookies):
        # valadate jwt
        try:
            data = auth.decode_jwt(cookies["files-jwt"])
        except:
            return responses.invalidtoken()
        
        # check if user has permission to list users
        if data["admin"] != True:
            return responses.insfperms()

        users = User.query.all()
        return responses.userlist(users)


    @use_args(wargs.logintoken, location="cookies")
    @use_args(wargs.adduser, location="json")
    def post(self, cookies, args):

        # valadate jwt
        try:
            data = auth.decode_jwt(cookies["files-jwt"])
        except:
            return responses.invalidtoken()
        
        # check if user has permission to add a user
        if data["admin"] != True:
            return responses.insfperms()

        key, salt = auth.hash(args["password"])
        new_user = User(username=args["username"], password=key, salt=salt, admin=args["admin"])
        db.session.add(new_user)

        try:
            db.session.commit()
        except Exception as e:
            e_msg = e.args[0]
            if 'duplicate key value violates unique constraint "user_username_key"' in e_msg:
                return responses.usertaken()
            else:
                raise e

        app.logger.info(f"New User: {new_user}")

        return responses.newuser()

    @use_args(wargs.logintoken, location="cookies")
    @use_args(wargs.deleteuser, location="json")
    def delete(self, cookies, args):
        
        # valadate jwt
        try:
            data = auth.decode_jwt(cookies["files-jwt"])
        except:
            return responses.invalidtoken()
        
        # check if user has permission to delete a user
        if data["admin"] != True:
            return responses.insfperms()

        # todo: check if user exists first
        for i in args["ids"]:
            User.query.filter((User.id==i)).delete()
            shutil.rmtree(os.path.join(app.config["UPLOAD_FOLDER"], str(i)))
        db.session.commit()

        os.rmdir(os.path.join(app.config["UPLOAD_FOLDER"], ))

        return responses.deleteuser()

class Files(Resource):

    def savefile(self, filedata, user_id, file_ttl):

        userfilepath = os.path.join(app.config['UPLOAD_FOLDER'], str(user_id))

        #get total size of user dir
        try:
            totalbytes = sum(os.path.getsize(f) for f in os.listdir(userfilepath) if os.path.isfile(f))
        except FileNotFoundError:
            os.mkdir(userfilepath)
            totalbytes = 0

        if totalbytes >= USER_QUOTA:
            raise QuotaLimit

        # check if user has exceeded file number limit
        if len(os.listdir(userfilepath)) >= MAX_USER_FILES:
            raise FileLimit

        filename = secure_filename(filedata.filename)
        fileid = str(uuid.uuid4())
        path = os.path.join(str(user_id), fileid)
        filepath = os.path.join(userfilepath, fileid)
        filedata.stream.seek(0)
        filedata.save(filepath)

        new_file = File(name=filename, path=path, mimetype=filedata.content_type, upload_time=int(time.time()), 
        user_id=user_id, size=os.path.getsize(filepath), expire_at=int(file_ttl))
        
        db.session.add(new_file)
        db.session.commit()


    @use_args(wargs.logintoken, location="cookies")
    def get(self, cookies):

        # valadate jwt
        try:
            token = auth.decode_jwt(cookies["files-jwt"])
        except:
            return responses.invalidtoken()

        items = File.query.filter_by(user_id=token["sub"]).order_by(File.upload_time.desc()).all()
        app.logger.info(items)
        return responses.filelist(items)

    @use_args(wargs.logintoken, location="cookies")
    @use_args(wargs.uploadfile, location="files")
    @use_args(wargs.fileoptions, location="form")
    def post(self, cookies, files, args):

        # valadate jwt
        try:
            token = auth.decode_jwt(cookies["files-jwt"])
        except:
            return responses.invalidtoken()

        files = request.files.getlist("file")
        for file in files:
            try:
                self.savefile(file, token["sub"], args.get("ttl", DEFAULT_TTL()))
                app.logger.debug(DEFAULT_TTL())
            except QuotaLimit:
                return responses.quotalimit()
            except FileLimit:
                return responses.filelimit()

        return responses.uploaded()

    @use_args(wargs.logintoken, location="cookies")
    @use_args(wargs.deletefile, location="json")
    def delete(self, cookies, args):
        # valadate jwt
        try:
            token = auth.decode_jwt(cookies["files-jwt"])
        except:
            return responses.invalidtoken()

        files = File.query.filter((File.user_id==token["sub"]) & (File.id.in_(args["ids"]))).all()
        
        for i in files:
            os.remove(os.path.join(app.config["UPLOAD_FOLDER"], i.path))

        File.query.filter((File.user_id==token["sub"]) & (File.id.in_(args["ids"]))).delete()
        db.session.commit()

        return responses.deletefile()

class DlFile(Resource):

    @use_args(wargs.fileurl, location="view_args")
    @use_args(wargs.logintoken, location="cookies")
    def get(self, args, cookies, **kwargs):
        # valadate jwt
        try:
            token = auth.decode_jwt(cookies["files-jwt"])
        except:
            return responses.invalidtoken()

        metadata = File.query.filter((File.id==args["id"]) & (File.user_id==token["sub"])).first()

        if metadata == None:
            return responses.insfperms()

        return send_file(os.path.join(app.config['UPLOAD_FOLDER'], metadata.path), 
        mimetype=metadata.mimetype, as_attachment=True, download_name=metadata.name)

api.add_resource(Login, "/login")
api.add_resource(Logout, "/logout")
api.add_resource(Users, "/users")
api.add_resource(Files, "/files")
api.add_resource(DlFile, "/files/<id>/<filename>")

sched = BackgroundScheduler(daemon=True)
sched.add_job(ttl_cleanup, "interval", minutes=30)
sched.start()

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
