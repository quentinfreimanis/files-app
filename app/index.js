function checkall() {

    const headcheck = document.getElementById("headcheck");
    const table = document.getElementById("main-table");

    // iterate over all body rows and toggle checkmark
    totalchecked = 0;
    for (var i = 1, row; row = table.rows[i]; i++) {
        row.cells[0].firstChild.checked = headcheck.checked;
        if (headcheck.checked) {
            totalchecked++;
        }
    }

    document.getElementById("del-btn").disabled = !headcheck.checked;

}

var totalchecked = 0;
function uncheck() {
    var headcheck = document.getElementById("headcheck");
    headcheck.checked = false;
    
    if (this.checked == true) {
        totalchecked++;
    } else if (this.checked == false) {
        totalchecked--;
    }

    if (totalchecked == 0) {
        document.getElementById("del-btn").disabled = true;
    } else {
        document.getElementById("del-btn").disabled = false;
    }

}

function addfile(item) {

    var tbodyRef = document.getElementById("main-table").getElementsByTagName("tbody")[0];

    // Insert a row at the end of table
    var newRow = tbodyRef.insertRow();

    // Insert cells at the end of the row
    var checkcell = newRow.insertCell();
    var namecell = newRow.insertCell();
    var ttlcell = newRow.insertCell();
    var datecell = newRow.insertCell();
    var dlcell = newRow.insertCell();

    var checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.onclick = uncheck;
    checkcell.appendChild(checkbox);


    // Append file name to row
    var newName = document.createTextNode(item.name);
    namecell.appendChild(newName);

    // Append expire time to row
    var ttl_hours = Math.floor((item.ttl - (Date.now() / 1000)) / 3600);
    var newTtl = document.createTextNode(ttl_hours + " Hours");
    ttlcell.appendChild(newTtl);

    // Append upload date to row
    var dateobj = new Date(item.upload_time*1000);
    var newDate = document.createTextNode(dateobj.toLocaleString());
    datecell.appendChild(newDate);

    // Append download button to cell
    var iconspan = document.createElement("span");
    iconspan.classList.add("material-icons");
    iconspan.appendChild(document.createTextNode("download"))
    var newDl = document.createElement("a");
    newDl.appendChild(iconspan);
    newDl.href="https://files.q-lab.dev/api/files/" + item.id + "/" + item.name;
    dlcell.appendChild(newDl);

    newRow.id = item.id;

}

async function getfilelist() {

    const response = await fetch("/api/files")
    const parsedJson = await response.json();

    if (response.status == 403 || response.status == 422) {
        window.location.href = "/about.html";
    }

    for (var i = 0; i < parsedJson.response.length; i++) {
        console.log(parsedJson.response[i]);
        addfile(parsedJson.response[i]);
    }

}

function cleartable() {
    var tbodyRef = document.getElementById("main-table").getElementsByTagName("tbody")[0];
    while (tbodyRef.firstChild) {
        tbodyRef.removeChild(tbodyRef.firstChild);
    }

    // clear table checkbox states
    uncheck();
    totalchecked = 0;
    document.getElementById("del-btn").disabled = true;
}


async function uploadfile(event) {
    const input = event.target;
    if ('files' in input && input.files.length > 0) {
        console.log(input.files[0]);

        let data = new FormData();
        data.append('file', input.files[0]);

        const response = await fetch("/api/files", {
            method: 'POST',
            body: data
        });

        const parsedJson = await response.json();
        console.log(parsedJson);

        cleartable();
        getfilelist();

    }
}

async function deletefiles() {

    const table = document.getElementById("main-table");
    let dellist = [];

    for (var i = 1, row; row = table.rows[i]; i++) {
        if (row.cells[0].firstChild.checked == true) {
            dellist.push(row.id);
        }
    }
    
    const fetchdata = {
        method: 'DELETE',
        body: JSON.stringify({ids: dellist}),
        headers: {
            'Content-Type': 'application/json'
          }
    }

    const response = await fetch("/api/files", fetchdata);
    const parsedJson = await response.json();

    if (response.status != 200) {
        console.log(parsedJson)
    }

    for (var i = 0; i < dellist.length; i++) {
        document.getElementById(dellist[i]).remove();
    }

    uncheck();
    totalchecked = 0;
    document.getElementById("del-btn").disabled = true;

}

// hide upload button when scrolling down
var lastScrollTop = window.scrollY || document.documentElement.scrollTop;
window.onscroll = function() {
    var st = window.scrollY || document.documentElement.scrollTop;
    if (st > lastScrollTop){
        document.getElementById("upload-btn").style.visibility="hidden";
    } else {
        document.getElementById("upload-btn").style.visibility="visible";
    }
    lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
}


document.getElementById('upload-file').addEventListener('change', uploadfile);

document.getElementById("headcheck").onclick = checkall;

document.getElementById("del-btn").onclick = deletefiles;

getfilelist();

// fix checkbox still being checked on page reload
uncheck();
document.getElementById("del-btn").disabled = true;