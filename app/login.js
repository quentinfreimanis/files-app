async function getlogin() {

    const uname = document.getElementById("username-input").value;
    const passwd = document.getElementById("password-input").value;

    const fetchdata = {
        method: 'POST',
        body: JSON.stringify({username: uname, password: passwd}),
        headers: {
            'Content-Type': 'application/json'
          }
    };

    const response = await fetch("https://files.q-lab.dev/api/login", fetchdata);
    const parsedJson = await response.json();

    if (response.status == 200) {
        window.location.href = "/";
    } else {
        console.log(parsedJson);
    }

}


window.onload = function() {

    const loginform = document.getElementById("login-form");
    loginform.onsubmit = getlogin;

}
