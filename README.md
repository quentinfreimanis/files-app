# files-app

A very simple cloud storage app

![](images/main_page.png)

The backend was made using Flask-RESTful, SQLAlchemy and PostgreSQL

The frontend is plain html, css and javascript
